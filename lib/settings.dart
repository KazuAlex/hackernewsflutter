import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

Route settingsRoute(SharedPreferences prefs) {
  return PageRouteBuilder(
    pageBuilder: (context, animation, secondaryAnimation)
        => SettingsPage(prefs: prefs),
    transitionsBuilder: (context, animation, secondaryAnimation, child) {
      const begin = Offset(1.0, 0.0);
      const end = Offset.zero;
      final tween = Tween(begin: begin, end: end);
      final offsetAnimation = animation.drive(tween);

      return SlideTransition(
        position: offsetAnimation,
        child: child,
      );
    },
  );
}

class SettingsPage extends StatefulWidget {
  const SettingsPage({
    Key? key,
    required this.prefs,
  }) : super(key: key);

  final SharedPreferences prefs;

  @override
  State<SettingsPage> createState() => _SettingsState();
}

class _SettingsState extends State<SettingsPage> {
  bool _openInWebView = false;

  @override
  void initState() {
    super.initState();
    _openInWebView = widget.prefs.getBool('openInWebView') ?? false;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Settings'),
        backgroundColor: Theme.of(context).primaryColor,
        foregroundColor: Colors.white,
      ),
      body: ListView(
        children: <Widget>[
          CheckboxListTile(
            title: const Text('Open links in app'),
            value: _openInWebView,
            activeColor: Theme.of(context).primaryColor,
            onChanged: (bool? value) {
              setState(() {
                _openInWebView = !_openInWebView;
                widget.prefs.setBool('openInWebView', _openInWebView);
              });
            },
          ),
        ],
      ),
    );
  }
}
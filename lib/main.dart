import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'settings.dart';
import 'hackernews/listPage.dart';

void main() {
  runApp(const App());
}

class App extends StatefulWidget {
  const App({Key? key}) : super(key: key);

  @override
  State<App> createState() => AppState();
}

class AppState extends State<App> {
  late SharedPreferences prefs;

  Future<SharedPreferences> loadPrefs() {
    return SharedPreferences.getInstance();
  }

  @override initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Hacker News',
      theme: ThemeData(
        brightness: Brightness.dark,
        primaryColor: Colors.orangeAccent.shade700,
        colorScheme: ColorScheme.fromSwatch().copyWith(
          brightness: Brightness.dark,
          secondary: Colors.grey,
          primary: Colors.white,
        ),
      ),
      home: FutureBuilder<SharedPreferences> (
        future: loadPrefs(),
        builder: (BuildContext context, AsyncSnapshot<SharedPreferences> snapshot) {
          if (!snapshot.hasData) {
            return const Center(
              child: CircularProgressIndicator(),
            );
          }
          prefs = snapshot.data!;
          return HackerNewsHome(prefs: prefs);
        }
      ),
      //home: HackerNewsHome(prefs: prefs!),
    );
  }
}

class HackerNewsHome extends StatefulWidget {
  const HackerNewsHome({
    Key? key,
    required this.prefs,
  }) : super(key: key);

  final SharedPreferences prefs;

  @override
  State<HackerNewsHome> createState() => HackerNewsHomeState();
}

class HackerNewsHomeState extends State<HackerNewsHome> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      initialIndex: 0,
      length: 5,
      child: Scaffold(
        body: NestedScrollView(
          headerSliverBuilder: (BuildContext context, bool innerBoxIsScrolled) {
            return <Widget>[
              SliverAppBar(
                title: const Text(
                  'Hacker News',
                  style: TextStyle(color: Colors.white),
                ),
                pinned: true,
                floating: true,
                actions: <Widget>[
                  IconButton(
                    icon: const Icon(Icons.settings),
                    color: Colors.white,
                    onPressed: () {
                      Navigator.of(context).push(settingsRoute(widget.prefs));
                    },
                  ),
                ],
                bottom: const TabBar(
                  indicatorColor: Colors.white,
                  tabs: <Widget>[
                    Tab(text: 'Top'),
                    Tab(text: 'New'),
                    Tab(text: 'Ask'),
                    Tab(text: 'Show'),
                    Tab(text: 'Jobs'),
                  ],
                ),
                backgroundColor: Theme.of(context).primaryColor,
              ),
            ];
          },
          body: TabBarView(
              children: <Widget>[
                ListWidget(type: 'top', prefs: widget.prefs),
                ListWidget(type: 'new', prefs: widget.prefs),
                ListWidget(type: 'ask', prefs: widget.prefs),
                ListWidget(type: 'show', prefs: widget.prefs),
                ListWidget(type: 'job', prefs: widget.prefs),
              ]
          ),
        ),
      ),
    );
  }
}
import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:validators/validators.dart';

const BASE_URL = 'https://hacker-news.firebaseio.com/v0/';

Map<int, http.Response> _itemsResp = {};
Map<int, Item> _itemsObj = {};

Future<Item> fetchItem(int id) async {
  _itemsResp[id] ??= await http.get(Uri.parse(BASE_URL + 'item/' + id.toString() + '.json'));

  if (_itemsResp[id]!.statusCode == 200) {
    _itemsObj[id] = Item.fromJson(jsonDecode(_itemsResp[id]!.body));
    return _itemsObj[id]!;
  } else {
    throw Exception('Failed to load Item');
  }
}

Item getItem(int id) {
  return _itemsObj[id]!;
}

class Item {
  final int id;
  final bool deleted;
  final String type;
  final String? by;
  final int? time;
  final bool dead;
  final List<int> kids;
  final int? parent;
  final String? text;
  final String? url;
  final String? title;
  final int descendants;
  final int score;

  const Item({
    required this.id,
    required this.deleted,
    required this.type,
    required this.by,
    required this.time,
    required this.dead,
    required this.kids,
    required this.parent,
    required this.text,
    required this.url,
    required this.title,
    required this.descendants,
    required this.score,
  });

  factory Item.fromJson(Map<String, dynamic> json) {
    return Item(
      id: json['id'],
      deleted: json['deleted'] ?? false,
      type: json['type'] ?? '',
      by: json['by'],
      time: json['time'],
      dead: json['dead'] ?? false,
      kids: List<int>.from(json['kids'] ?? []),
      parent: json['parent'],
      text: json['text'],
      url: json['url'],
      title: json['title'],
      descendants: json['descendants'] ?? 0,
      score: json['score'] ?? 0,
    );
  }

  bool isAsk() {
    return !isURL(url) && type == 'story';
  }

  List<int> sublistKids(int start) {
    return kids.sublist(start, start + 10);
  }
}

class Items {
  final List<int> ids;

  const Items({
    required this.ids,
  });

  List<int> sublist(int start) {
    return ids.sublist(start, start + 10);
  }

  int length() {
    return ids.length;
  }

  factory Items.fromJson(List<int> json) {
    return Items(
      ids: json,
    );
  }
}
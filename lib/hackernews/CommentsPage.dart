import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'model/item.dart';
import 'comments.dart';

Route commentsRoute(Item item, SharedPreferences prefs) {
  return PageRouteBuilder(
    pageBuilder: (context, animation, secondaryAnimation) => CommentsPage(item: item, prefs: prefs),
    transitionsBuilder: (context, animation, secondaryAnimation, child) {
      const begin = Offset(1.0, 0.0);
      const end = Offset.zero;
      final tween = Tween(begin: begin, end: end);
      final offsetAnimation = animation.drive(tween);

      return SlideTransition(
        position: offsetAnimation,
        child: child,
      );
    },
  );
}

class CommentsPage extends StatefulWidget {
  const CommentsPage({
    Key? key,
    required this.item,
    required this.prefs,
  }) : super(key: key);

  final Item item;
  final SharedPreferences prefs;

  @override
  State<CommentsPage> createState() => _CommentsState();
}

class _CommentsState extends State<CommentsPage> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Comments'),
        backgroundColor: Theme.of(context).primaryColor,
        foregroundColor: Colors.white,
      ),
      body: ListView(
        children: <Widget>[
          CommentsWidget(item: widget.item, prefs: widget.prefs),
        ],
      ),
    );
  }
}
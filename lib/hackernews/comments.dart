import 'package:flutter/material.dart';
import 'package:validators/validators.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:timeago/timeago.dart' as timeago;
import 'package:shared_preferences/shared_preferences.dart';
import 'package:flutter_html/flutter_html.dart';

import 'model/item.dart';

class CommentsWidget extends StatefulWidget {
  const CommentsWidget({
    Key? key,
    required this.item,
    required this.prefs,
  }) : super(key: key);

  final Item item;
  final SharedPreferences prefs;

  @override
  State<CommentsWidget> createState() => _CommentsState();
}

class _CommentsState extends State<CommentsWidget> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return ListView.separated(
      physics: const NeverScrollableScrollPhysics(),
      key: PageStorageKey('comments_${widget.item.id}'),
      itemCount: widget.item.kids.length,
      itemBuilder: (BuildContext context, int index) {
        return CommentWidget(id: widget.item.kids[index], prefs: widget.prefs);
      },
      separatorBuilder: (BuildContext context, int index) => const Divider(height: 5),
      shrinkWrap: true,
    );
  }
}

// single comment

class CommentWidget extends StatefulWidget {
  const CommentWidget({
    Key? key,
    required this.id,
    required this.prefs,
  }) : super(key: key);

  final int id;
  final SharedPreferences prefs;

  @override
  State<CommentWidget> createState() => _CommentState();
}

class _CommentState extends State<CommentWidget> {
  Item? _item;

  late Future<Item> futureItem;
  bool showComments = false;

  void openLink(String? url) async {
    if (url != null && isURL(url)) {
      await launch(
        url,
        forceWebView: widget.prefs.getBool('openInWebView') ?? false,
      );
    }
  }

  @override
  void initState() {
    super.initState();
    futureItem = fetchItem(widget.id);
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder<Item>(
      future: futureItem,
      builder: (context, snapshot) {
        if (snapshot.hasData) {
          _item = snapshot.data;
          if (_item != null && !_item!.dead && !_item!.deleted) {
            const subtitleFont = TextStyle(
              color: Colors.grey,
              fontSize: 12.0,
            );

            var subtitle = <TextSpan>[];
            if (_item!.by != null) {
              subtitle.add(TextSpan(
                style: subtitleFont,
                text: '${_item!.by} ',
              ));
            }
            if (_item!.time != null) {
              subtitle.add(TextSpan(
                  style: subtitleFont,
                  text: timeago.format(
                      DateTime.fromMillisecondsSinceEpoch(_item!.time! * 1000)
                  )
              ));
            }
            if (_item!.kids.isNotEmpty) {
              subtitle.add(TextSpan(
                style: subtitleFont,
                text: ' | ${_item!.kids.length} comment${_item!.kids.length != 1 ? 's' : ''}'
              ));
            }

            SelectableHtml content = SelectableHtml(
              data: _item?.text ?? 'N/A',
              onLinkTap: (String? url, RenderContext context, Map<String, String> attributes, element) => openLink(url),
            );

            var children = <Widget>[
              Container(
                padding: const EdgeInsets.only(
                  left: 8.0,
                  top: 12.0,
                  right: 8.0,
                ),
                child: Text.rich(TextSpan(children: subtitle)),
              ),
              Container(
                padding: const EdgeInsets.all(8.0),
                child: content,
              ),
            ];

            if (_item!.kids.isNotEmpty && showComments) {
              children.add(Container(
                padding: const EdgeInsets.only(
                  left: 18.0,
                ),
                child: CommentsWidget(item: _item!, prefs: widget.prefs),
              ));
            }

            return GestureDetector(
              onTap: () {
                setState(() { showComments = !showComments; });
              },
              child: ListView(
                shrinkWrap: true,
                physics: const NeverScrollableScrollPhysics(),
                children: children,
              )
            );
          } else if (_item != null) {
            return const Text('Deleted');
          } else {
            return const Text('N/A');
          }
        } else if (snapshot.hasError) {
          return Text('${snapshot.error}');
        }
        return const Center(
            child: CircularProgressIndicator()
        );
      },
    );
  }
}
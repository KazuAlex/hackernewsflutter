import 'package:flutter/material.dart';
import 'package:timeago/timeago.dart' as timeago;
import 'package:shared_preferences/shared_preferences.dart';
import 'package:flutter_html/flutter_html.dart';

import 'model/item.dart';
import 'comments.dart';

Route askRoute(Item item, SharedPreferences prefs) {
  return PageRouteBuilder(
    pageBuilder: (context, animation, secondaryAnimation) => AskPage(item: item, prefs: prefs),
    transitionsBuilder: (context, animation, secondaryAnimation, child) {
      const begin = Offset(1.0, 0.0);
      const end = Offset.zero;
      final tween = Tween(begin: begin, end: end);
      final offsetAnimation = animation.drive(tween);

      return SlideTransition(
        position: offsetAnimation,
        child: child,
      );
    },
  );
}

class AskPage extends StatefulWidget {
  const AskPage({
    Key? key,
    required this.item,
    required this.prefs,
  }) : super(key: key);

  final Item item;
  final SharedPreferences prefs;

  @override
  State<AskPage> createState() => _AskState();
}

class _AskState extends State<AskPage> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    var title = <TextSpan>[];
    var subtitle = <TextSpan>[];
    SelectableHtml? text;

    const subtitleFont = TextStyle(
      color: Colors.grey,
      fontSize: 12.0,
    );

    title.add(TextSpan(
      text: widget.item.title ?? 'N/A',
      style: const  TextStyle(
        color: Colors.white,
        fontSize: 16.0,
      ),
    ));

    if (widget.item.score >= 0) {
      subtitle.add(TextSpan(
        style: subtitleFont,
        text: ' ${widget.item.score.toString()} points',
      ));
    }
    if (widget.item.by != null) {
      subtitle.add(TextSpan(
        style: subtitleFont,
        text: ' by ${widget.item.by}')
      );
    }
    if (widget.item.time != null) {
      subtitle.add(TextSpan(
        style: subtitleFont,
        text: ' ' + timeago.format(
            DateTime.fromMillisecondsSinceEpoch(widget.item.time! * 1000)
        )
      ));
    }

    if (widget.item.text != null) {
      text = SelectableHtml(data: widget.item.text ?? '');
    }

    return Scaffold(
      appBar: AppBar(
        title: const Text('Ask'),
        backgroundColor: Theme.of(context).primaryColor,
        foregroundColor: Colors.white,
      ),
      body: ListView(
        children: <Widget>[
          Container(
            padding: const EdgeInsets.only(
              left: 8.0,
              top: 8.0,
              right: 8.0,
              bottom: 0.0,
            ),
            child: Text.rich(
              TextSpan(children: title),
            ),
          ),
          Container(
            padding: const EdgeInsets.only(
              left: 8.0,
              top: 0.0,
              right: 8.0,
              bottom: 8.0,
            ),
            child: Text.rich(
              TextSpan(children: subtitle),
            ),
          ),
          Container(
            padding: const EdgeInsets.all(8.0),
            child: text ?? Html(data: ''),
          ),
          Center(
            child: Container(
              padding: const EdgeInsets.only(
                top: 8.0,
              ),
              child: const Text(
                'Comments',
                style: TextStyle(
                  fontSize: 18.0,
                ),
              )
            )
          ),
          CommentsWidget(item: widget.item, prefs: widget.prefs),
        ],
      ),
    );
  }
}
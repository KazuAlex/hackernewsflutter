import 'dart:convert';
import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';
import 'package:flutter_slidable/flutter_slidable.dart';

import 'item.dart';
import 'model/item.dart';
import 'commentsPage.dart';

http.Response? response;

final endpoints = {
  'top': 'topstories.json',
  'new': 'newstories.json',
  'ask': 'askstories.json',
  'show': 'showstories.json',
  'job': 'jobstories.json',
};

Future<Items> fetchList(endpoint) async {
  response ??= await http.get(Uri.parse(BASE_URL + endpoints[endpoint]!));

  if (response!.statusCode == 200) {
    final List<dynamic> jsonD = jsonDecode(response!.body);
    final List<int> json = jsonD.map((i) => i as int).toList();
    return Items.fromJson(json);
  } else {
    throw Exception('Failed to load $endpoint');
  }
}

class ListWidget extends StatefulWidget {
  const ListWidget({
    Key? key,
    required this.type,
    required this.prefs,
  }): super(key: key);

  final String type;
  final SharedPreferences prefs;

  @override
  State<ListWidget> createState() => _ListState();
}

class _ListState extends State<ListWidget> {
  Items? _items;
  final _readed = <int>[];
  final _itemIds = <int>[];

  late Future<Items> futureItems;

  void _handleRead(int id) {
    if (!_readed.contains(id)) {
      setState(() {
        _readed.add(id);
      });
    }
  }

  void openComments(BuildContext context, Item item) {
    Navigator.of(context).push(commentsRoute(item, widget.prefs));
  }

  void upvote(BuildContext context, Item item) {
    log('upvote ${item.id.toString()}');
    ScaffoldMessenger.of(context).showSnackBar(
        const SnackBar(
          content: Text(
            'Upvote is not implemented yet.',
            style: TextStyle(
              color: Colors.white,
            ),
          ),
        )
    );
  }

  void favorite(BuildContext context, Item item) {
    log('favorite ${item.id.toString()}');
    const snackBar = SnackBar(
      content: Text(
          'Favorite is not implemented yet.',
          style: TextStyle(
            color: Colors.white,
          )
      ),
    );
    ScaffoldMessenger.of(context).showSnackBar(snackBar);
  }

  @override
  void initState() {
    super.initState();
    futureItems = fetchList(widget.type);
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder<Items>(
        future: futureItems,
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            _items = snapshot.data;
            return ListView.separated(
              padding: const EdgeInsets.all(0.0),
              key: PageStorageKey(widget.type),
              addAutomaticKeepAlives: true,
              itemCount: _items?.length() ?? 0,
              itemBuilder: (BuildContext context, int index) {
                if (index >= _itemIds.length) {
                  _itemIds.addAll(_items?.sublist(_itemIds.length) ?? []);
                }

                return Slidable(
                  startActionPane: ActionPane(
                    motion: const StretchMotion(),
                    children: [
                      SlidableAction(
                        onPressed: (BuildContext context) {
                          favorite(context, getItem(_itemIds[index]));
                        },
                        backgroundColor: Colors.red.shade600,
                        foregroundColor: Colors.white,
                        icon: Icons.favorite,
                        label: 'Favorite',
                      ),
                      SlidableAction(
                        onPressed: (BuildContext context) {
                          upvote(context, getItem(_itemIds[index]));
                        },
                        // backgroundColor: const Color(0xFF21B7CA),
                        backgroundColor: Colors.lightBlueAccent,
                        foregroundColor: Colors.white,
                        icon: Icons.thumb_up,
                        label: 'Upvote',
                      ),
                    ],
                  ),
                  endActionPane: ActionPane(
                    motion: const StretchMotion(),
                    children: [
                      SlidableAction(
                        onPressed: (BuildContext context) {
                          openComments(context, getItem(_itemIds[index]));
                        },
                        backgroundColor: Colors.blueGrey.shade400,
                        foregroundColor: Colors.white,
                        icon: Icons.comment_outlined,
                        label: 'Comments',
                      ),
                    ],
                  ),
                  child: ItemWidget(
                    id: _itemIds[index],
                    readed: _readed.contains(_itemIds[index]),
                    onRead: _handleRead,
                    prefs: widget.prefs,
                  ),
                );
              },
              separatorBuilder: (BuildContext context, int index) => const Divider(height: 5),
            );
          } else if (snapshot.hasError) {
            return Text('${snapshot.error}');
          }
          return const Center(
              child: CircularProgressIndicator()
          );
        }
    );
  }
}
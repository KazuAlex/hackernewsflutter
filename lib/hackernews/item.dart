import 'package:flutter/material.dart';
import 'package:validators/validators.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:timeago/timeago.dart' as timeago;
import 'package:shared_preferences/shared_preferences.dart';

import 'model/item.dart';
import 'askPage.dart';

class ItemWidget extends StatefulWidget {
  const ItemWidget({
    Key? key,
    required this.id,
    required this.readed,
    required this.onRead,
    required this.prefs,
  }): super(key: key);

  final int id;
  final bool readed;
  final ValueChanged<int> onRead;
  final SharedPreferences prefs;
  static List<String>? _readedList;

  @override
  State<ItemWidget> createState() => ItemState();
}

class ItemState extends State<ItemWidget> {
  final _font = const TextStyle(
    color: Colors.white,
    fontSize: 16.0,
  );
  final _readedFont = const TextStyle(
    color: Colors.grey,
    fontSize: 16.0,
  );
  final _uriFont = const TextStyle(
    color: Colors.grey,
    fontSize: 11.0,
  );

  Item? _item;
  bool readed = false;

  late Future<Item> futureItem;

  @override
  void initState() {
    super.initState();
    futureItem = fetchItem(widget.id);

    ItemWidget._readedList ??= widget.prefs.getStringList('itemsReaded') ?? [];
    readed = ItemWidget._readedList!.contains('${widget.id}');
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder<Item>(
        future: futureItem,
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            _item = snapshot.data;

            var title = <TextSpan>[];
            var subtitle = <TextSpan>[];
            if (_item != null) {
              // title
              title.add(TextSpan(
                text: _item!.title ?? 'N/A',
                style: readed ? _readedFont : _font,
              ));

              if (_item!.url != null && isURL(_item!.url)) {
                var uri = Uri.parse(_item!.url!);
                title.add(TextSpan(
                  text: ' (${uri.host})',
                  style: _uriFont,
                ));
              }

              // subtitle
              if (_item!.score > 0) {
                subtitle.add(
                  TextSpan(text: ' ${_item!.score.toString()} points')
                );
              }
              if (_item!.by != null) {
                subtitle.add(TextSpan(text: ' by ${_item!.by}'));
              }
              if (_item!.time != null) {
                subtitle.add(TextSpan(
                  text: ' ' + timeago.format(
                    DateTime.fromMillisecondsSinceEpoch(_item!.time! * 1000)
                  )
                ));
              }
              if (_item!.descendants > 0) {
                subtitle.add(TextSpan(text: ' | ${_item!.descendants} comment${_item!.descendants != 1 ? 's' : ''}'));
              }
            } else {
              title.add(const TextSpan(text: 'N/A'));
            }

            return ListTile(
              dense: true,
              contentPadding: const EdgeInsets.only(left: 8.0, right: 8.0),
              title: Text.rich(
                TextSpan(
                  children: title,
                ),
              ),
              subtitle: Text.rich(
                  TextSpan(
                    children: subtitle,
                  )
              ),
              onTap: () async {
                if (
                  isURL(_item?.url)
                  && await launch(
                    _item!.url!,
                    forceWebView: widget.prefs.getBool('openInWebView') ?? false,
                  )
                ) {
                  setState(() {
                    if (!readed) {
                      readed = true;
                      widget.onRead(widget.id);
                      ItemWidget._readedList!.add('${widget.id}');
                      widget.prefs.setStringList('itemsReaded', ItemWidget._readedList!);
                    }
                  });
                } else if (_item != null && _item!.isAsk()) {
                  Navigator.of(context).push(askRoute(_item!, widget.prefs));
                }
              },
            );
          } else if (snapshot.hasError) {
            return Text('${snapshot.error}');
          }
          return const Center(
              child: CircularProgressIndicator()
          );
        }
    );
  }
}
